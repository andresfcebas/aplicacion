﻿
using Realms;

namespace Aplicacion.App.model {
    public class Model_User : RealmObject  {        
     
        [PrimaryKey]
        public int Id { get; set; }    
        
        public string Name { get; set; }        
        public string Avatar_url { get; set; }
    }
}