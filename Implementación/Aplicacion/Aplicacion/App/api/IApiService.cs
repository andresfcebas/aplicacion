﻿
using Aplicacion.App.model;
using Refit;
using System.Threading.Tasks;

namespace Aplicacion.App.api {


    [Headers ("User-Agent: :request:")]
    interface IApiService {

        [Get("/{user}")]
        Task<Model_User> GetUser(string user);

    }
}