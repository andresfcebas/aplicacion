﻿using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Aplicacion.App.api;
using Aplicacion.App.db;
using Aplicacion.App.model;
using Aplicacion.App.utils;
using Refit;
using System.Collections.Generic;
using System.Net;

namespace Aplicacion.App.ui.main {
    [Activity(Label = "@string/app_name", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTop, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity, IMainView {

        MainPresenter presenter;
        EditText txtEntrada;
        Button leerJson;
        TextView txtSalida;
        ImageView imgAvatar;
        Model_User userSession;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            presenter = new MainPresenter();
            SetContentView(Resource.Layout.activity_main);            

            AppDatabaseRealm bases = new AppDatabaseRealm();           

            txtEntrada = FindViewById<EditText>(Resource.Id.txtEntrada);
            leerJson = FindViewById<Button>(Resource.Id.btnJson);
            txtSalida = FindViewById<TextView>(Resource.Id.txtSalida);
            imgAvatar = FindViewById<ImageView>(Resource.Id.imgAvatar);

            List<Model_User> temp = presenter.GetDBUser();

            if(temp.Count > 0) {
                userSession = temp[0];
                LoadData();
            }
            
            leerJson.Click += async delegate {

                userSession = await presenter.GetApiUser(txtEntrada.Text);
                userSession.Id = 0;
                presenter.InsertDBUser(userSession);
                LoadData();

            }; 
        }

        private void LoadData() {
            txtSalida.Text = " Nombre: " + userSession.Name;
            Bitmap avatarBitmap = GetImageUrl(userSession.Avatar_url);
            imgAvatar.SetImageBitmap(avatarBitmap);
        }

        private Bitmap GetImageUrl(string url) {
            Bitmap imageBitmap = null;
            using (var webClient = new WebClient()) {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0) {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;
        }

        public Model_User GetUserByApi(string user) {
            throw new System.NotImplementedException();
        }

        public Model_User GetUserByDB(long id) {
            throw new System.NotImplementedException();
        }
    }
}

