﻿
using Aplicacion.App.api;
using Aplicacion.App.db;
using Aplicacion.App.model;
using Aplicacion.App.utils;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aplicacion.App.ui.main {
    public class MainRepository {

        private IApiService ApiService { get; set; }
        private UserDAO UserDAO { get; set; }       

        public MainRepository() {
            UserDAO = new UserDAO();
            ApiService = RestService.For<IApiService>(Constants.URL_BASE);
        }   

        public async Task<Model_User> CallingMethod(string name) {
            return await ApiService.GetUser(name);
        }

        public void InsertUserDB(Model_User user) {
            UserDAO.Insert(user);
        }

        public List<Model_User> GetUserDB(long id) {
            return UserDAO.GetById(id);
        }

    }
}