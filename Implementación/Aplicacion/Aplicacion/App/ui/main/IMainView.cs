﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Aplicacion.App.model;

namespace Aplicacion.App.ui.main {
    public interface IMainView {

        Model_User GetUserByApi(string user);
        Model_User GetUserByDB(long id);

    }
}