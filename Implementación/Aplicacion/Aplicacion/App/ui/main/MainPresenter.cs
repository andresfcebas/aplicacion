﻿
using Android.Graphics;
using Aplicacion.App.model;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Aplicacion.App.ui.main {

    public class MainPresenter {

        public MainRepository Repositorio { get; set; }

        public MainPresenter() {
            Repositorio = new MainRepository();
        }

        public async Task<Model_User> GetApiUser(string user) {
            return await Repositorio.CallingMethod(user);
        }

        public List<Model_User> GetDBUser() {
            return Repositorio.GetUserDB(0);
        }

        public void InsertDBUser(Model_User user) {
            Repositorio.InsertUserDB(user);
        }
                
    }
}
