﻿
using Aplicacion.App.model;
using Realms;
using System.Collections.Generic;
using System.Linq;

namespace Aplicacion.App.db {

    public class UserDAO : IDAO {

        Realm database;

        public UserDAO() {
            database = AppDatabaseRealm.database;
        }

        public List<Model_User> GetById(long id) {
            return database.All<Model_User>().Where(d => d.Id == 0).ToList<Model_User>();
            //return database.All<Model_User>().Where(d => d.Id == 0).First();
        }

        public void Insert(Model_User user) {
            database.Write(() => {
                Model_User users = database.Add(user, true);
            });
        }
    }
}