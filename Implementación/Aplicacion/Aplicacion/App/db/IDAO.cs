﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Aplicacion.App.model;

namespace Aplicacion.App.db {
    interface IDAO {

        void Insert(Model_User user);
        List<Model_User> GetById(long id);

    }
}